import os, sys
from os import listdir
import pickle

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.layout import Layout
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.clock import Clock

from hat_types import HatGame, HatWord, HatPlayer


sys.path.append(os.path.dirname(__file__))

kv_path = './kv/'
for kv in listdir(kv_path):
    Builder.load_file(kv_path + kv)
Builder.load_file('main.kv')

se, e, m, h, sh = pickle.load(open('diffs.pickle', 'rb'))
to_d = {'Очень сложно': sh,
        'Сложно': h,
        'Где-то посередине': m,
        'Просто': e,
        'Очень просто': se,
        }


GAME = None


class HatLabel(Label):
    pass


class HatTextInput(TextInput):
    pass


class HatNumTextInput(HatTextInput, FocusBehavior):
    pass


class HatLayout(Layout):
    pass


class HatBoxLayout(HatLayout, BoxLayout):
    pass


class StartLayout(HatBoxLayout):
    pass


class SetLayout(HatBoxLayout):

    def play(self):
        global GAME
        players = self.players.text.split('\n')
        difficulty = self.difficulty
        n = int(self.n.text)

        GAME = HatGame(players, to_d[difficulty.text], n, mode=difficulty)
        self.parent.manager.current = 'pass'


class PassLayout(HatBoxLayout):
    def update(self):
        self.player_text.text = f'{GAME.players[GAME.cur].name} - {GAME.players[GAME.listener].name}'


class ExplainLayout(HatBoxLayout):
    def start(self):
        self.t = GAME.duration
        self.time.text = str(self.t)
        self.event = Clock.schedule_interval(self.tick, 1)
        self.choose_word()
        self.word_text.text = self.word

    def choose_word(self):
        self.word = GAME.pick_word()

    def tick(self, dt):
        self.t -= 1
        self.time.text = str(self.t)

    def stop(self):
        self.event.cancel()

    def guessed(self):
        GAME.words[self.word].n_of_tries += 1
        GAME.players[GAME.cur].explained += 1
        GAME.players[GAME.listener].guessed += 1
        GAME.finished_words[self.word] = GAME.words[self.word]
        GAME.words.pop(self.word)

        if len(GAME.words) == 0:
            self.parent.manager.current = 'result'
            return

        if self.t <= 0:
            GAME.next_player()
            self.parent.manager.current = 'pass'
            return

        self.choose_word()
        self.word_text.text = self.word

    def skip(self):
        GAME.words[self.word].n_of_tries += 1
        GAME.next_player()
        self.parent.manager.current = 'pass'


class ResultLayout(HatBoxLayout):
    def show_results(self):
        for player in sorted(GAME.players, key=lambda x: -(x.explained + x.guessed)):
            for item in (player.name, str(player.explained), str(player.guessed)):
                self.grid.add_widget(HatLabel(text=item))

    def remove_results(self):
        for i in self.grid.children[:-3]:
            self.grid.remove_widget(i)


class HatScreenManager(ScreenManager):
    pass


class HatApp(App):

    def build(self):
        self.title = 'Бесконечная Шляпа'
        sm = HatScreenManager()
        return sm


if __name__ == "__main__":
    app = HatApp()
    app.run()
