import random
from typing import (
    List,
    Dict,
)


class HatPlayer:
    def __init__(self, name, explained, guessed):
        self.name: str = name
        self.explained: int = explained
        self.guessed: int = guessed


class HatWord:
    def __init__(self, n_of_tries, etime):
        self.n_of_tries: int = 0
        self.etime: float = 0


class HatGame:
    def __init__(self, players, words, n, mode):
        self.players: List[HatPlayer] = [HatPlayer(name, 0, 0) for name in players]
        self.words: Dict[str, HatWord] = {word: HatWord(0, 0) for word in random.sample(words, n)}
        self.finished_words: Dict[str, HatWord] = dict()
        self.n: int = n
        self.mode: str = mode
        self.cur: int = 0
        self.step: int = 1
        self.duration: int = 20

    @property
    def listener(self):
        return (self.cur + self.step) % len(self.players)

    def pick_word(self):
        return random.choice(list(self.words.keys()))

    def next_player(self):
        self.cur = (self.cur + 1) % len(self.players)
        if self.cur == 0:
            self.step = self.step + 1
            if self.step == len(self.players):
                self.step = 1
